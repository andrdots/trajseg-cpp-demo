cmake_minimum_required(VERSION 3.5)

project(trajseg-cpp-demo LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_subdirectory(lib)

declare_executable(trajseg-cpp-demo)

target_sources(trajseg-cpp-demo
PRIVATE
    "${CMAKE_CURRENT_SOURCE_DIR}/main.cpp"
)

target_link_libraries(trajseg-cpp-demo
PRIVATE
    trajseg-cpp
)

protect_target(trajseg-cpp-demo)

install(TARGETS
    trajseg-cpp-demo
LIBRARY
DESTINATION
    ${CMAKE_INSTALL_LIBDIR}
)
