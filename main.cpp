/*
 * This is a test for experience and skills from one of the potencial employers from Russia for a
 * junior C++ developer (previously I had little experience with the STL library). Do not try to
 * understand the meaning of all of that code. It doesn't have any.
 *
 * This code will be fully rewritten afterwords and its contents partially will be moved to tests
 * (fuzzy testing).
 */

#include <iostream>

// Task 1
#include <trajseg-cpp/Arc.h>
#include <trajseg-cpp/Ellipse.h>
#include <trajseg-cpp/Helix.h>
#include <trajseg-cpp/Trajectory.h>

#include <iostream>
#include <memory>
#include <random>
#include <utility>

using namespace std;
using namespace trajseg;
using namespace advcpp;
using namespace in4D;

int
main()
{
    std::default_random_engine rndEngine;
    std::uniform_int_distribution<> distribition(2, 4);
    std::uniform_real_distribution<double> axisDistribution(-100.0, 100.0);
    std::uniform_real_distribution<double> kDistribution(-1.0, 1.0);
    std::uniform_real_distribution<double> rDistribution(0.1, 100.0);
    std::uniform_real_distribution<double> angleDistribution(0.0, M_PI * 2);

    Trajectory<double> container1;
    Point lastPoint(0.0, 0.0, 0.0);
    for (int i = 0; i < 100; ++i) {
        Point nextPoint;
        Segment<double> *seg = nullptr;
        int type = distribition(rndEngine);
        switch (type) {
        case Ellipse<double>::ClassId: {
            Point center(
                axisDistribution(rndEngine),
                axisDistribution(rndEngine),
                axisDistribution(rndEngine));
            Vector normal(
                kDistribution(rndEngine),
                kDistribution(rndEngine),
                kDistribution(rndEngine));
            normal = normal * (lastPoint - center);
            normal.normalize();
            Vector majorDir(
                kDistribution(rndEngine),
                kDistribution(rndEngine),
                kDistribution(rndEngine));
            majorDir = majorDir * normal;
            double majorRadius = rDistribution(rndEngine);
            double minorRadius = rDistribution(rndEngine);
            double angle = ellipse::GetParameterAngleByPoint(
                lastPoint,
                center,
                normal,
                majorDir,
                majorRadius,
                minorRadius);
            auto p = ellipse::GetPointByParameterAngle(
                angle,
                center,
                normal,
                majorDir,
                majorRadius,
                minorRadius);
            center = center - (p - lastPoint);
            nextPoint = ellipse::GetPointByParameterAngle(
                angle + angleDistribution(rndEngine),
                center,
                normal,
                majorDir,
                majorRadius,
                minorRadius);
            seg = new Ellipse<double>(
                lastPoint,
                nextPoint,
                center,
                normal,
                majorDir,
                majorRadius,
                minorRadius,
                0.001);
            break;
        }
        case Arc<double>::ClassId: {
            Point middlePoint(
                axisDistribution(rndEngine),
                axisDistribution(rndEngine),
                axisDistribution(rndEngine));
            nextPoint = Point(
                axisDistribution(rndEngine),
                axisDistribution(rndEngine),
                axisDistribution(rndEngine));
            seg = new Arc(lastPoint, middlePoint, nextPoint);
            break;
        }
        case Helix<double>::ClassId: {
            Point center(
                axisDistribution(rndEngine),
                axisDistribution(rndEngine),
                axisDistribution(rndEngine));
            Vector normal(
                kDistribution(rndEngine),
                kDistribution(rndEngine),
                kDistribution(rndEngine));
            normal = normal * (lastPoint - center);
            normal.normalize();
            double height = rDistribution(rndEngine);
            double step = height / rDistribution(rndEngine);
            seg = new Helix(lastPoint, center, normal, step, height);
            nextPoint = seg->end();
            break;
        }
        }
        container1.emplaceBack(seg);

        lastPoint = nextPoint;
    }

    // Task 3
    for (const shared_ptr<Segment<double>> &segPtr : std::as_const(container1)) {
        // M_PI / 4 is not a len, and this angle probably might not be inside segment bounds
        auto p = segPtr->at(M_PI / 4);      // I was said to do so, it was against my will!
        auto d = segPtr->tangent(M_PI / 4); // I was said to do so, was is against my will!
        if (p.x != p.x) {
            abort();
        }
        cout << segPtr->className() << "(" << segPtr->segmentClassId() << "): (" << p.x << "; "
             << p.y << "; " << p.z << ") / " << "{" << d.x << "; " << d.y << "; " << d.z << "}"
             << endl;
    }

    cout << "Total length: " << container1.len() << endl;

    // Task 4
    vector<shared_ptr<Segment<double>>> container2;
    copy_if(
        container1.begin(),
        container1.end(),
        back_inserter(container2),
        [](shared_ptr<Segment<double>> segPtr) {
            return (dynamic_cast<Arc<double> *>(segPtr.get()) != nullptr);
        });

    // Task 5
    sort(
        container2.begin(),
        container2.end(),
        [](shared_ptr<Segment<double>> seg1, shared_ptr<Segment<double>> seg2) {
            auto arc1 = dynamic_cast<const Arc<double> *>(seg1.get());
            auto arc2 = dynamic_cast<const Arc<double> *>(seg2.get());
            return (arc1->radius() < arc2->radius());
        });
    for (auto &segPtr : container2) {
        auto arc = dynamic_cast<Arc<double> *>(segPtr.get());
    }

    // Task 6
    double rSum = 0;
#pragma omp parallel for reduction(+ : rSum)
    for (auto &segPtr : container2) {
        auto arc = dynamic_cast<Arc<double> *>(segPtr.get());
        rSum += arc->radius();
    }
    cout << "Total radius sum: " << rSum << endl;

    return 0;
}
