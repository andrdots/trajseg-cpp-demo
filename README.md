# Trajectory Segments C++ demo

## Description
The project is just a demonstration of the trajseg-cpp library.

## License
The demonstration project is licensed under the MIT No Attribution license (MIT-0).

## Status
Currently, the project is just a test for the experience from potential employer (for the junior C++ developer vacation). The implementation differs from initial test requirements to be suitable for real life usage, e. g. additional requirements have been added. The test doesn't specify which trajectory parameter should be used, but for interpolation purposes trajectory parameter must be position rather than angle. The angle will cause trajectory increments within ellipse different in length, so it cannot be used to move along a trajectory at specified speed with the same precision (an ellipse with big radii difference will require line interpolation between points). Also, the angle parameter is the same for different radii. The detalization level for a small and big circles will be significantly different. This will make rough lines visible in ellipse if it will be visualized in 3D. So angle is not suitable neither for the interpolation nor for the 3D visualization. Another added requirement is that all the primitives must form trajectory that can be used to determine point coordinates and derivative by trajectory parameter (position).
